#+TITLE: Practice artefact for Quick Sort
#+AUTHOR: VLEAD
#+DATE: [02-12-2019]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =quick sort practice= interactive
  artefact(HTML).

* Features of the artefact
  + Artefact provides practice for simple quick sort.
  + User can select a array size and enter an array in the
    text box provided to practice the artefact.
  + User shall click the =Setup= button to start the
    practice artefact from the beginning again.
  + User can click the =Reset= button to restart the demo.
  + User can click on =Next= manually to view the demo at
    his/her own speed.

* HTML Framework of Selection Sort
** Head Section Elements
Title, meta tags and all the links related to CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
  <title>Quick Sort Practice</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../../static/lib/JSAV/css/JSAV.css" type="text/css" />
  <link rel="stylesheet" href="../../static/lib/ODSA/css/odsaAV.css" type="text/css" />
  <link rel="stylesheet" href="../../static/lib/ODSA/css/odsaStyle.css" type="text/css" />
  <link rel="stylesheet" href="../../static/lib/quicksortAV.css" type="text/css" />
  <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
  <link rel="stylesheet" type="text/css" href="../css/quicksort-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li><b>Select</b> the length of the array which you
      would like to sort in the select pane.</li>
      <li>Enter the unsorted array where each elements are
      separated by commas in the input field.</li>
      <li>Click <b>Setup</b> to draw the canvas and start
      the experiment.</li>
      <li>Click <b>Next</b> to go ahead with the
      visualization.</li>
      <li>Click on <b>Reset</b> to reset the experiment and
      start again.</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Input Holder
#+NAME: input-holder
#+BEGIN_SRC html
<div id="input-holder">
  <label id="arraysizeLabel" for="arraysize"></label>
  <select id="arraysize"></select>
  <label id="arrayValuesLabel" for="arrayValues"></label>
  <input size="25" name="arrayValues" id="arrayValues" class="text-box" placeholder="Array Values" type="text" />
</div>

#+END_SRC

*** Array Holder
This is a array holder, where a list of random numbers to be
sorted is shown using a quick sort algorithm visualization.
#+NAME: array-holder
#+BEGIN_SRC html
<div class="avcontainer">
    <div class="jsavcontrols" style="display: none;"></div>
    <p class="jsavoutput jsavline" style="display: none;"></p>
</div> 

#+END_SRC

*** Legend
This is a legend, that can referred to when observing.
#+NAME: legend
#+BEGIN_SRC html
<ul class="legend">
  <li><span class="left"></span> Left Pointer </li>
  <li><span class="right"></span> Right Pointer </li>
  <li><span class="merge"></span> Merge of Pointers</li>
  <li><span class="selected-pivot"></span> Selected Pivot Element</li>
  <li><span class="swapping"></span> Swapping</li>
</ul>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed, based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div class="comment-box" id="comment-box-bigger">
  <b>Observations</b>
  <p id="ins"></p>
</div>

#+END_SRC

*** Buttons Wrapper
This is a div wrapped with buttons used for
performing/demonstrating an artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <form id="ssperform" style="display: none">
    <p>
      <input id="run" type="button" name="run" />
      <input id="reset" type="button" name="reset" />
    </p>
  </form>
  <input class="button-input" type="button" value="Setup" id="setup">
  <input class="button-input" type="button" value="Reset" id="resetArt">
  <input class="button-input" type="button" value="Next" id="next">
</div>

#+END_SRC

** Javascript
This comprises of all the Javascript needed to run or add
behavior to the artefact.
#+NAME: js-elems
#+BEGIN_SRC html
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="../../static/lib/JSAV/lib/jquery.transit.js"></script>
<script src="../../static/lib/JSAV/js/JSAV-min.js"></script>
<script src="../../static/lib/ODSA/js/odsaUtils.js"></script>
<script src="../../static/lib/ODSA/js/odsaAV.js"></script>
<script src="../../static/lib/quicksortCODE.js"></script>
<script src="../js/quicksortAV.js"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
<script>
  function handlers(){
    $("#setup").click(function() {
      displayComment("");
      commentsQueue = [];
      $("#run").click();
    });

    $("#next").click(function() {
      $(".jsavforward").click();
      displayComment(commentsQueue.shift());
    });
        
    $("#resetArt").click(function() {
      $("#reset").click();
      displayComment("");
      commentsQueue = [];
    });
        
    $('#setup').click();
  }
</script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle quicksort-practice.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body onload="handlers();">
    <<instruction-box>>
    <div id="container">
       <<input-holder>>
       <<array-holder>>
       <div id="legend-comment-wrapper">
          <<legend>>
          <<comments-box>>
       </div>
       <<buttons-wrapper>>
    </div>
    <<js-elems>>
  </body>
</html>
#+END_SRC
