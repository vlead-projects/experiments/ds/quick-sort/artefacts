#+TITLE: Common Styles for Quick Sort Interactive artefacts
#+AUTHOR: VLEAD
#+DATE: [02-12-2019]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the common styles which, can be used
  across all the html interactive artefacts.

* Styles
** CSS for main container
#+NAME: style-container
#+BEGIN_SRC css
#container {
  position: relative;
  width: auto;
  height: auto;
  overflow-y: visible;
  overflow-x: hidden;
  padding-top: 2%;
  border: 0;
  background-color: white !important;
}

#+END_SRC

** CSS for input holder
#+NAME: style-input-holder
#+BEGIN_SRC css
#input-holder {
  text-align: center;
  padding-top: 1%;
  padding-bottom: 2%;
}

#arraysize {
  height: 41px;
}

#+END_SRC

** CSS for Question Holder
#+NAME: style-question-holder
#+BEGIN_SRC css
#question-holder {
  font-size: 1.5vw;
}

#+END_SRC
** CSS for array visualization container
#+NAME: style-visualization-container
#+BEGIN_SRC css
.avcontainer{
  overflow: visible;
}

/* Styles to resize array boxes */
.jsavhorizontalarray .jsavnode {
    line-height: 50px !important;
    min-width: 50px !important;
    max-width: 50px !important;
    min-height: 50px !important;
    max-height: 50px !important;
}

/* Styles to position arrows */
.jsavindex.jsavarrow::before {
    left: 18px;
}

#+END_SRC

** CSS for legend and comments wrapper
#+NAME: style-legend-comments-wrapper
#+BEGIN_SRC css
#legend-comment-wrapper {
    padding-top: 2%;
    height: 27vh;
}

#+END_SRC

** CSS for legend
#+NAME: style-legend
#+BEGIN_SRC css
.legend {
    float: left;
}

.left {
   background-color: #0000ff;
}

.right {
   background-color: #ff0000;
}

.merge {
   background-color: #cc00ff;
}

.selected-pivot {
   background-color: #ddddff;
}

.swapping {
   background-color: #f99b9b;
}

#+END_SRC

** CSS for comments box wrapper which gives feedback and comments
#+NAME: style-comments-box
#+BEGIN_SRC css
.comment-box {
    margin: auto;
}

#+END_SRC

** CSS for slider container 
#+NAME: style-slider-container
#+BEGIN_SRC css
.slidecontainer {
    padding-top: 0%;
}

#+END_SRC

** CSS for buttons wrapper
#+NAME: style-buttons-wrapper
#+BEGIN_SRC css
.buttons-wrapper {
    text-align: center;
    padding-top: 2%;
}

#+END_SRC

* Tangle
#+BEGIN_SRC css :tangle quicksort-styles.css :eval no :noweb yes
<<style-body>>
<<style-container>>
<<style-input-holder>>
<<style-question-holder>>
<<style-visualization-container>>
<<style-legend-comments-wrapper>>
<<style-legend>>
<<style-comments-box>>
<<style-slider-container>>
<<style-buttons-wrapper>>
#+END_SRC
